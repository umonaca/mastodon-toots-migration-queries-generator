'use strict';
const beautify = require("json-beautify");
const strip = require('strip');
const htmlentities = require('htmlentities');
const fs = require('fs');
const path = require('path');
const filePath = path.join(__dirname, 'outbox.json');

const MastodonTootsMigrationQueriesGenerator = {
    config: {
        accountId: 2,
        oldInstance: 'oldmastodoninstance.social',
        oldUserName: 'bob',
        removeTags: false,
    },

    setConfig: function () {
        const args = process.argv;
        args.shift();
        args.shift();
        const helpMessage = `Required arguments missing.\nCommand usage: node revert.js <accountId> <oldInstanceUrl> <oldUserName>\nExample: node revert.js ${this.config.accountId} ${this.config.oldInstance} ${this.config.oldUserName} [--removetags]`;
        const warnMessage = '\nOn a production server you may not want to remove tags, because other statuses may contain those imported tags!'
        if(args.length < 3 || args.length > 4) {
            console.log( helpMessage );
            console.log( warnMessage );
            return false;
        }

        this.config.accountId = args[0];
        this.config.oldInstance = args[1];
        this.config.oldUserName = args[2];
        if (args[3] === '--removetags') {
            this.config.removeTags = true;
        }
        return true;
    },

    getId: function (item) {
        return item.id.split('/')[6];
    },

    getDate: function (item) {
        return item.published
            .replace('T', ' ')
            .replace('Z', '.000');
    },

    getQueries: function (items) {
        if(!this.setConfig()) {
            return false;
        }
        const itemsWithoutReplies = items.filter(item => {
            return item.object.inReplyTo === null || (item.object.inReplyTo && item.object.inReplyTo.startsWith(`https://${this.config.oldInstance}/users/${this.config.oldUserName}`));            
        });
        console.log('BEGIN;');
        this.getStatuses(itemsWithoutReplies);
        if (this.config.removeTags) {
            this.getTags(itemsWithoutReplies);
        }
        console.log('COMMIT;');
    },

    getStatuses: function (items) {
        items.map( item => {
            const id = this.getId(item);
            const str = htmlentities.decode(strip(item.object.content));
            const re = /\&apos;/gi;
            const text = str.replace(re, "''");
            const cw = item.object.summary;
            const visibility = this.getStatusVisibility(item);
            if (visibility !== 3) {
                if(!!cw) {
                    //const query = `INSERT INTO statuses (id, uri, text, created_at, updated_at, sensitive, visibility, spoiler_text, local, account_id, application_id) VALUES ('${id}', '${uri}', '${text}', '${date}', '${date}', 'f', '${visibility}', '${cw}', 't', '${this.config.accountId}', '1');`;
                    const query = `DELETE FROM statuses WHERE id = ${id};`;
                    console.log( query );
                } else if(text) {
                    //const query = `INSERT INTO statuses (id, uri, text, created_at, updated_at, sensitive, visibility, local, account_id, application_id) VALUES ('${id}', '${uri}', '${text}', '${date}', '${date}', 'f', '${visibility}', 't', '${this.config.accountId}', '1');`;
                    const query = `DELETE FROM statuses WHERE id = ${id};`;
                    console.log( query );
                }
            }
        });
    },

    getTags: function (items) {
        const datas = [];
        items.map( item => {
            const id = this.getId(item);
            item.object.tag.map(tag => {
                if(typeof(datas[tag.type]) === 'undefined') {
                    datas[tag.type] = [];
                }
                tag.id = id;
                tag['created'] = this.getDate(item);
                datas[tag.type].push(tag)
            });
        });
        const tagsSet = new Set();
        const queries = [];
        const tagsToInsert = datas['Hashtag'].map(tag => {
            if(!tagsSet.has(tag.name)) {
                tagsSet.add(tag.name);
                //queries.push(`INSERT INTO tags (name, created_at, updated_at) VALUES ('${tag.name.replace('#', '')}', '${tag.created}', '${tag.created}');`);
                queries.push(`DELETE FROM tags WHERE name = \'${tag.name.replace('#', '')}\';`);
            }
            //queries.push(`INSERT INTO statuses_tags (status_id, tag_id) VALUES ('${tag.id}', (select id from tags where name ='${tag.name.replace('#', '')}'));`);
            //statuses_tags are automatically deleted by on delete cascade constraint 
        });
        queries.map(query => console.log(query));
    },

    getStatusVisibility: function (item) {
        if (this.isVisibilityPublic(item)) {
            return 0;
        }
        if (this.isVisibilityUnlisted(item)) {
            return 1;
        }
        if (this.isVisibilityFollowersOnly(item)) {
            return 2;
        }
        // Mastodon does not have full support of limited visibility (circle/aspect) as of v3.2.0
        // Every other visibility types are treated as direct messages here
        return 3;
    },

    isVisibilityPublic: function (item) {
        return item.to[0] === 'https://www.w3.org/ns/activitystreams#Public' && item.cc.length > 0 && item.cc[0].endsWith('/followers');
    },

    isVisibilityUnlisted: function (item) {
        return item.to.length > 0 && item.to[0].endsWith('/followers') && item.cc[0] === 'https://www.w3.org/ns/activitystreams#Public';
    },

    isVisibilityFollowersOnly: function (item) {
        return item.to.length > 0 && item.to[0].endsWith('/followers') && item.cc[0] !== 'https://www.w3.org/ns/activitystreams#Public';
    },
}

if (fs.existsSync(filePath)) {
    fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
        const content = JSON.parse(data);
        process.argv.length > 2 && process.argv[2] === 'beautify' ?
            console.log( beautify(content, null, 2, 100) ) :
            MastodonTootsMigrationQueriesGenerator.getQueries(content.orderedItems);
    });
} else {
    console.log( `You must put your exported file from Mastodon at this location: ${filePath}` );
}
