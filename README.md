# Mastodon Toots Migration Queries Generator

## Warning

BEFORE DOING ANYTHING, BACKUP YOUR DATABASE!

## About

This is a quick and dirty migration tool developed by Simounet a year ago. I rewrote them to accomodate recent Mastodon version. It should work for v3.2.0.  
Toots containing replies ~~or mentions~~, boosts or direct messages are removed on purpose, because it would be strange to see conversations from an other location.

## Under the hood

- Fill in the `statuses`, `tags`, `statuses_tags` tables
- Replace the old instance references with the new one
- Replace the old username with the new username in status URI
- Generates from `outbox.json` a file containing all the queries needed that you can import into your psql database

## Requirements

- NodeJS > 6
- npm >= 5.6.0

## Changelog since the upstream version

- Removed streaming entries because Mastodon no longer supports OStatus since 3.0
- Included spoiler text support
- Fixed confusing instructions by replacing 'user id' with 'account id'
- No longer requires the lastStatusId. It is not suitable for a running production server with constantly increasing number of hashtags.
- Added required command line parameter to replace the old username with new username in status URI
- Wrapped insert statements in a transaction block
- Fixed direct messages treated as public
- Fixed a bug that allows only the first toot of a long thread to be imported
- Do not insert a tag when the unique constraint is violated
- Added a queries generator `revert.js` to revert changes

## Install

$ npm install

## Usage

You may have to change the `user`, `table prefix` and `database` names for `psql` commands.

### Get the user id from target instance

`$ psql mastodon -c "SELECT id FROM accounts WHERE username = 'targetUserName';"`

It is the same id as in the uri: `newinstance.com/web/accounts/12345`.

### Generate the queries file

1. Get the `outbox.json` file from your Mastodon's instance (`/settings/export`) and put it into this folder
2. `$ node index.js <accountId> <oldInstanceUrl> <newInstanceUrl> <oldUserName> <newUserName>`  
  Example: `node index.js 2 oldmastodoninstance.social newmastodoninstance.social bob alice > queries.sql`

### Import the queries intro your Mastodon database

`$ psql --host localhost --port 5432 --username mastodon_user mastodon_databases < queries.sql`

You can use some other SQL tools such as pgAdmin to execute queries. Make sure you **examine and understand** the query file before you  
execute it.

## Known issues

1. If a toot is a reply to an author who deleted their account, or the in-reply-to toot was deleted by the original author, your reply will not be  
  recognized as a reply in a conversation/thread. They will be imported as if they are seperate toots.
2. All toots in threads that you created will be imported, but as seperate toots. They are no longer linked together in a thread in order to  
  avoid database constraint issues.

## Revert changes

All the queries are wrapped inside a transaction block, which means that either all of them are successfully executed or nothing's changed.

However, if you do want to revert a **successful** import, you can either manually do it on your own or use the `revert.js`.  
The number of lines containing `DELETE FROM statuses` should be exactly the same as in the previous import queries.

Do not use the `revert.js` unless you have already done a `successful` import. If you have seen an error message during a previous import,  
  your database is unchanged and safe to go.

### Usage

`$ node revert.js <accountId> <oldInstanceUrl> <oldUserName> [--removetags] > queries_revert.sql`

Example:

`node revert.js 2 oldmastodoninstance.social bob > queries_revert.sql`

or

`node revert.js 2 oldmastodoninstance.social bob --removetags > queries_revert.sql`

It's not recommended to use `--removetags` on a production server, because there's a high chance that existing statuses contain one or more  
tags that you imported.

## Feel free to contribute

I know this is not perfect. If you have any idea to make it better, feel free to make a merge request to this repository.
