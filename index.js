'use strict';
const beautify = require("json-beautify");
const strip = require('strip');
const htmlentities = require('htmlentities');
const fs = require('fs');
const path = require('path');
const filePath = path.join(__dirname, 'outbox.json');

const MastodonTootsMigrationQueriesGenerator = {
    config: {
        accountId: 2,
        oldInstance: 'oldmastodoninstance.social',
        newInstance: 'newmastodoninstance.social',
        oldUserName: 'bob',
        newUserName: 'alice',
    },

    setConfig: function () {
        const args = process.argv;
        args.shift();
        args.shift();
        const helpMessage = `Required arguments missing.\nCommand usage: node index.js <accountId> <oldInstanceUrl> <newInstanceUrl> <oldUserName> <newUserName>\nExample: node index.js ${this.config.accountId} ${this.config.oldInstance} ${this.config.newInstance} ${this.config.oldUserName} ${this.config.newUserName}`;
        if(args.length !== 5) {
            console.log( helpMessage );
            return false;
        }
        
        this.config.accountId = args[0];
        this.config.oldInstance = args[1];
        this.config.newInstance = args[2];
        this.config.oldUserName = args[3];
        this.config.newUserName = args[4];
        return true;
    },

    getId: function (item) {
        return item.id.split('/')[6];
    },

    getDate: function (item) {
        return item.published
            .replace('T', ' ')
            .replace('Z', '.000');
    },

    getQueries: function (items) {
        if(!this.setConfig()) {
            return false;
        }
        const itemsWithoutReplies = items.filter(item => {
            return item.object.inReplyTo === null || (item.object.inReplyTo && item.object.inReplyTo.startsWith(`https://${this.config.oldInstance}/users/${this.config.oldUserName}`));            
        });
        console.log('BEGIN;');
        this.getStatuses(itemsWithoutReplies);
        this.getTags(itemsWithoutReplies);
        console.log('COMMIT;');
    },

    getStatuses: function (items) {
        items.map( item => {
            const id = this.getId(item);
            const uri = item.id
                .replace(this.config.oldInstance, this.config.newInstance)
                .replace(this.config.oldUserName, this.config.newUserName)
                .replace('/activity', '');
            const date = this.getDate(item);
            const str = htmlentities.decode(strip(item.object.content));
            const re = /\&apos;/gi;
            const text = str.replace(re, "''");
            const cw = item.object.summary;
            const visibility = this.getStatusVisibility(item);
            if (visibility !== 3) {
                if(!!cw) {
                    const query = `INSERT INTO statuses (id, uri, text, created_at, updated_at, sensitive, visibility, spoiler_text, local, account_id, application_id) VALUES ('${id}', '${uri}', '${text}', '${date}', '${date}', 'f', '${visibility}', '${cw}', 't', '${this.config.accountId}', '1');`;
                    console.log( query );
                } else if(text) {
                    const query = `INSERT INTO statuses (id, uri, text, created_at, updated_at, sensitive, visibility, local, account_id, application_id) VALUES ('${id}', '${uri}', '${text}', '${date}', '${date}', 'f', '${visibility}', 't', '${this.config.accountId}', '1');`;
                    console.log( query );
                }
            }
        });
    },

    getTags: function (items) {
        const datas = [];
        items.map( item => {
            const id = this.getId(item);
            item.object.tag.map(tag => {
                if(typeof(datas[tag.type]) === 'undefined') {
                    datas[tag.type] = [];
                }
                tag.id = id;
                tag['created'] = this.getDate(item);
                datas[tag.type].push(tag)
            });
        });
        const tagsSet = new Set();
        const queries = [];
        const tagsToInsert = datas['Hashtag'].map(tag => {
            if(!tagsSet.has(tag.name)) {
                tagsSet.add(tag.name);
                queries.push(`INSERT INTO tags (name, created_at, updated_at) VALUES ('${tag.name.replace('#', '')}', '${tag.created}', '${tag.created}') ON CONFLICT DO NOTHING;`);
            }
            queries.push(`INSERT INTO statuses_tags (status_id, tag_id) VALUES ('${tag.id}', (select id from tags where name ='${tag.name.replace('#', '')}'));`);
        });
        queries.map(query => console.log(query));
    },

    getStatusVisibility: function (item) {
        if (this.isVisibilityPublic(item)) {
            return 0;
        }
        if (this.isVisibilityUnlisted(item)) {
            return 1;
        }
        if (this.isVisibilityFollowersOnly(item)) {
            return 2;
        }
        // Mastodon does not have full support of limited visibility (circle/aspect) as of v3.2.0
        // Every other visibility types are treated as direct messages here
        return 3;
    },

    isVisibilityPublic: function (item) {
        return item.to[0] === 'https://www.w3.org/ns/activitystreams#Public' && item.cc.length > 0 && item.cc[0].endsWith('/followers');
    },

    isVisibilityUnlisted: function (item) {
        return item.to.length > 0 && item.to[0].endsWith('/followers') && item.cc[0] === 'https://www.w3.org/ns/activitystreams#Public';
    },

    isVisibilityFollowersOnly: function (item) {
        return item.to.length > 0 && item.to[0].endsWith('/followers') && item.cc[0] !== 'https://www.w3.org/ns/activitystreams#Public';
    },
}

if (fs.existsSync(filePath)) {
    fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
        const content = JSON.parse(data);
        process.argv.length > 2 && process.argv[2] === 'beautify' ?
            console.log( beautify(content, null, 2, 100) ) :
            MastodonTootsMigrationQueriesGenerator.getQueries(content.orderedItems);
    });
} else {
    console.log( `You must put your exported file from Mastodon at this location: ${filePath}` );
}
